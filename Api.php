<?php
class Api{
    private $_ch;
    private $_token;
    private $_domain;
    private $_version = "v0.1";

    function __construct($token, $domain = "mtargetapi.ru", $debug = false) {
        $this -> _debug = $debug;
        $this -> _domain = $domain;
        $this -> _token = $token;
		$this -> _ch = curl_init();
    }

	public function info(){
		return $this -> api("info");
	}

	public function lead($data = false){
		return $this -> api("lead", $data);
	}

	public function request($data){
		return $this -> api("request", $data);
	}

	protected function api($method, $data = false) {
		try {
	        return $this -> _query("http://".$this -> _domain . "/api?".http_build_query(array(
	        	"token" => $this->_token,
	        	"method" => $method,
	        	"version" => $this->_version
	        )), $data);
		} catch (Exception $e) {
			exit("Ошибка: ".$e->getMessage().PHP_EOL);
		}
    }
	
	private function _query($url, $data = false) {
		if($this->_debug){
			echo "Load: ".$url."\n";
		}
		curl_setopt($this -> _ch, CURLOPT_URL, $url);
		curl_setopt($this -> _ch, CURLOPT_HEADER, false);
		curl_setopt($this -> _ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($this -> _ch, CURLOPT_NOBODY, 0);
		curl_setopt($this -> _ch, CURLOPT_RETURNTRANSFER, true);
		if($data){
			if($this->_debug){
				echo "Post data:";
				print_r($data);
			}
			curl_setopt($this -> _ch, CURLOPT_POST, true);
			curl_setopt($this -> _ch, CURLOPT_HTTPHEADER, array(                                                                          
				"Expect:"
			));
			curl_setopt($this -> _ch, CURLOPT_POSTFIELDS, http_build_query($data));
		}
		$page = curl_exec($this -> _ch);
		if($this->_debug){
			print_r(curl_getinfo($this -> _ch));
		}
		$data = json_decode($page, true);
		if(empty($data)) throw new Exception(($this->_debug?$page:"При получении данных с сервера"));
		return $data;
	}

	function __destruct() {
		if ($this -> _ch)
			curl_close($this -> _ch);
	}
}
